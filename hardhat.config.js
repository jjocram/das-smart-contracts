require("@nomiclabs/hardhat-waffle");
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.4",
  networks: {
    local: {
        url: 'http://localhost:8545',
        chainId: 1074,
        accounts: [process.env.USER_PRIVATE_KEY],
        timeout: 60000
    }
  }
};
