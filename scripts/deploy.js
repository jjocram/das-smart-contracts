// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const AddressSharing = await hre.ethers.getContractFactory("AddressSharing");
  const addressSharingContract = await AddressSharing.deploy();
  await addressSharingContract.deployed();

  const SSPermissions = await hre.ethers.getContractFactory("SSPermissions");
  const ssPermissionsContract = await SSPermissions.deploy();
  await ssPermissionsContract.deployed();

  console.log("Save the following addresses in .env file")
  console.log("AddressSharing deployed to:", addressSharingContract.address);
  console.log("SSPermissions deployed to:", ssPermissionsContract.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
