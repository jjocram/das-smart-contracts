//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract AddressSharing {
  struct User {
    address ethaddr;
    bool created;
  }

  mapping (bytes32 => User) users;

  // Takes the username as a 32bit hash
  function set_user(bytes32 username) public {
    require(
        users[username].created == false,
        "User already registered."
        );
    users[username].ethaddr = msg.sender;
    users[username].created = true;
  }

  // Takes the username as a 32bit hash
  function get_user(bytes32 username) public view returns (User memory) {
    return users[username];
  }
}


